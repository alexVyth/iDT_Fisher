
"""
Script to train a basic action classification system.

Trains a One vs. Rest SVM classifier on the fisher vector video outputs.
This script is used to experimentally test different parameter settings for the SVMs.

"""

import os, sys, collections, random, string
import numpy as np
from tempfile import TemporaryFile
from sklearn.pipeline import Pipeline
from sklearn.svm import LinearSVC
from sklearn import svm
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import cross_validate
import classify_library
from classify_library import plot_learning_curve, metric_mAP, metric_AP
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC



class_index_file = "./class_index.npz"
training_output = './train'
testing_output = './test'

class_index_file_loaded = np.load(class_index_file)
class_index = class_index_file_loaded['class_index'][()]
index_class = class_index_file_loaded['index_class'][()]


# In[7]:


training = [filename for filename in os.listdir(training_output) if filename.endswith('.fisher.npz')]
testing = [filename for filename in os.listdir(testing_output) if filename.endswith('.fisher.npz')]


training_dict = classify_library.toDict(training)
testing_dict = classify_library.toDict(testing)
#GET THE TRAINING AND TESTING DATA.


X_train_vids, X_test_vids = classify_library.limited_input(training_dict,
                                                           testing_dict,
                                                           5, 5000)
X_train_vids = sorted(X_train_vids)
X_test_vids = sorted(X_test_vids)
X_train_vids = X_train_vids[::12]
X_test_vids = X_test_vids[::12]
X_train, Y_train = classify_library.make_FV_matrix(X_train_vids,
                                                   training_output,
                                                   class_index)
X_test, Y_test = classify_library.make_FV_matrix(X_test_vids, testing_output,
                                                 class_index)
#
estimator = OneVsRestClassifier(LinearSVC(random_state=0, C=100,
                                          loss='hinge', penalty='l2'))

classifier = estimator.fit(X_train, Y_train)
y_pred = estimator.predict(X_test)
print(confusion_matrix(Y_test, y_pred))
target_names = ['Immobility', 'Swimming', 'Climbing', 'Head Shaking', 'Diving']
print(classification_report(Y_test, y_pred, target_names=target_names))
