# extract IDT features

import numpy as np
import os
import subprocess
import sys

"""
Wrapper library for the IDTF executable.
Implements methods to extract IDTFs.
Seperate methods for extracting IDTF and computing Fisher Vectors.

Example usage:
python computeIDTF.py video_list.txt output_directory
"""
# Path to the video repository

# Improved Dense Trajectories binary
dtBin = '/home/alex/miniconda3/envs/tj/improved_trajectory_release/release/DenseTrackStab'

# Temp directory to store resized videos

COMPUTE_FV = 'python ./computeFVstream.py'


def extract(videoName, outputBase):
    """
    Extracts the IDTFs and stores them in outputBase file.
    """
    if not os.path.exists(videoName):
        print '%s does not exist!' % videoName
        return False
    subprocess.call('%s %s > %s' % (dtBin, videoName, outputBase), shell=True)
    return True


def extractFV(videoName, outputBase, gmm_list):
    """
    Extracts the IDTFs, constructs a Fisher Vector, and saves the Fisher Vector at outputBase
    outputBase: the full path to the newly constructed fisher vector.
    gmm_list: file of the saved list of gmms
    """
    if not os.path.exists(videoName):
        print('%s does not exist!' % videoName)
        return False
    subprocess.call('%s %s | %s %s %s' % (dtBin, videoName, COMPUTE_FV,
                                          outputBase, gmm_list), shell=True)
    return True
