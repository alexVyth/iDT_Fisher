#!/bin/bash

UCF_DIR="/home/alex/Desktop/mouse/mouse_dataset"
TRAIN_LIST="/home/alex/Desktop/mouse/trainlist01.txt"
GMM_OUT="/home/alex/Desktop/iDT_Fisher/gmm_list"
#
# python gmm.py 128 $UCF_DIR $TRAIN_LIST $GMM_OUT --pca

trainlist01="/home/alex/Desktop/mouse/trainlist01.txt"
testlist01="/home/alex/Desktop/mouse/testlist01.txt"
#
training_output="/home/alex/Desktop/iDT_Fisher/train"
testing_output="/home/alex/Desktop/iDT_Fisher/test"
#
python computeFVs.py $UCF_DIR $trainlist01 $training_output $GMM_OUT
python computeFVs.py $UCF_DIR $testlist01 $testing_output $GMM_OUT

CLASS_INDEX="/home/alex/Desktop/mouse/classInd.txt"
CLASS_INDEX_OUT="./class_index"
# python compute_UCF101_class_index.py $CLASS_INDEX $CLASS_INDEX_OUT
# #
# python classify_experiment.py
